package com.jeesuite.amqp;

/**
 * 
 * <br>
 * Class Name   : MessageStatus
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2020年6月29日
 */
public enum MessageStatus {

	notExists,unprocessed,processed
}
