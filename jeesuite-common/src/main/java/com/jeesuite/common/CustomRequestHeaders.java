package com.jeesuite.common;

public class CustomRequestHeaders {

	// header
		public static final String HEADER_PREFIX = "x-";
		public static final String HEADER_REAL_IP = "x-real-ip";
		public static final String HEADER_FROWARDED_FOR = "x-forwarded-for";
		public static final String HEADER_AUTH_TOKEN = "x-auth-token";
		public static final String HEADER_AUTH_USER = "x-auth-user";
		public static final String HEADER_TENANT_ID = "x-tenant-id";
		public static final String HEADER_CLIENT_TYPE = "x-client-type";
		public static final String HEADER_REQUESTED_WITH = "x-requested-with";
		public static final String HEADER_FORWARDED_HOST = "x-forwarded-host";
		public static final String HEADER_FORWARDED_PROTO = "x-forwarded-proto";
		public static final String HEADER_FORWARDED_PORT = "x-forwarded-port";
		public static final String HEADER_FORWARDED_PRIFIX = "x-forwarded-prefix";
		public static final String HEADER_GATEWAY_TOKEN = "x-gateway-token";
		public static final String HEADER_SESSION_ID = "x-session-id";
		public static final String HEADER_SESSION_EXPIRE_IN = "x-session-expire-in";
		public static final String HEADER_INVOKER_IP = "x-invoker-ip";
		public static final String HEADER_INTERNAL_REQUEST = "x-internal-request";
	    public static final String HEADER_INVOKER_APP_ID = "x-invoker-appid";
	    public static final String HEADER_REQUEST_ID = "x-request-id";
		public static final String HEADER_RESP_KEEP = "x-resp-keep";
		public static final String HEADER_HTTP_STATUS_KEEP = "x-httpstatus-keep";
		public static final String HEADER_VERIFIED_MOBILE = "x-verified-mobile";
}
